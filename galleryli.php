<!DOCTYPE html>	
<head>
<title>小光點藝廊</title>
<!-- 社群連結fb/line -->
<meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="小光點畫廊 Spotlight gallery" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="" />
<meta property="og:image:height" content="" />

<?php require('newhead.php') ?>
<script language="javascript">
// 動畫效果
$(document).ready(function() {

});
</script>
<body>
    <!-- loading動畫 -->
    <div class="js-patLoadingAniBg patLoading">
    </div>

    <?php require('newheader.php') ?>

    <div class="whiteHelfBgArea">
        <!-- <div class="yellowHelfBg"></div> -->

        <!-- 畫廊列表 -->
        <div class="patSection pagGalliBk">
            <!-- 側邊欄位區 -->
            <aside class="modAside">
                <!-- 麵包屑 -->
                <article class="eleBite mb-15">
                    <a href="index.php" class="eleBite-link">
                        首頁
                    </a>
                    <a href="galleryli.php" class="eleBite-link">
                        線上畫廊
                    </a>
                    <a href="index.php" class="eleBite-link">
                        油畫
                    </a>
                </article>
                <!-- title -->
                <div class="modAsideAside-titArea">
                    <h2 class="modLetterAniArea">
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">S</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">p</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">o</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">t</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">l</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">i</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">g</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">h</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">t</span>
                        <span class="modLetterAniArea-blank"></span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">G</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">a</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">l</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">l</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">e</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">r</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">y</span>
                    </h2>
                    <h1 class="typo-chTit mt-5 modLetterAniArea">
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">線</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">上</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">藝</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">廊</span>
                    </h1>
                </div>
                <div class="pagGalliAside-emBtArea mt-50">
                    <a href="javascript:(0);" class="btnGalleryIntro-emText--text wow">
                        你是這世界的溫暖光點<br />讓我們牽起手照亮世界
                    </a>
                </div>
                <!-- 側邊分類欄 -->
                <div class="pagGalliAside-sideBk">
                    <h6 class="pagGalliAside-sideBk--sectionTit mb-20">Sort by material</h6>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link pagGalliAside-sideBk--link--act" title="全部">全部</a>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="油畫">油畫</a>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="水彩">水彩</a>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="壓克力顏料">壓克力顏料</a>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="粉蠟筆">粉蠟筆</a>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="綜合媒材">綜合媒材</a>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="色鉛筆">色鉛筆</a>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="水墨">水墨</a>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="麥克筆">麥克筆</a>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="廣告顏料">廣告顏料</a>
                </div>
                <div class="pagGalliAside-sideBk">
                    <h6 class="pagGalliAside-sideBk--sectionTit mb-20">Sort by price</h6>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="$3000 ― $10000">$3000 ― $10000</a>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="$10000 ― $20000">$10000 ― $20000</a>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="$20000 ― $30000">$20000 ― $30000</a>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="$20000 ― $50000">$20000 ― $50000</a>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="$50000 ― $80000">$50000 ― $80000</a>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="$80000 ― $100000">$80000 ― $100000</a>
                    <a href="galleryli.php" class="pagGalliAside-sideBk--link" title="$100000 ―">$100000 ―</a>
                </div>
            </aside>

            <!-- 畫作列表區 -->
            <div class="pagGalliList">
                <ul class="pagGalliList-column">
                    <!-- 一頁放12個 -->
                    <!-- 一個畫作 -->
                    <li class="mod-galleryli animate__animated animate__fadeIn wow">
                        <!-- 建議尺寸 -->
                        <!-- 建議尺寸 1440 * auto -->
                        <a href="galleryin.php" class="mod-galleryli-imgBg">
                            <img src="images/newgallery01.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                        </a>
                        <h3 class="mod-galleryli-tit">
                            <a href="galleryin.php">星星小丑</a>
                        </h3>
                        <section class="mod-galleryli-info">
                            <h5 class="mb-5">Artist</h5>
                            <h6 class="mb-10">林依柔－唐氏症</h6>
                            <h5 class="mb-5">Material</h5>
                            <h6 class="mb-10">簽字筆</h6>
                            <h5 class="mb-5">Size</h5>
                            <h6 class="mb-10">130cm＊100cm</h6>
                        </section>
                    </li>
                    <li class="mod-galleryli animate__animated animate__fadeIn wow">
                        <!-- 建議尺寸 -->
                        <a href="galleryin.php" class="mod-galleryli-imgBg">
                            <img src="images/newgallery02.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                        </a>
                        <h3 class="mod-galleryli-tit">
                            <a href="galleryin.php">心田花園</a>
                        </h3>
                        <section class="mod-galleryli-info">
                            <h5 class="mb-5">Artist</h5>
                            <h6 class="mb-10">謝建鋒－自閉症與馬凡氏症</h6>
                            <h5 class="mb-5">Material</h5>
                            <h6 class="mb-10">油畫</h6>
                            <h5 class="mb-5">Size</h5>
                            <h6 class="mb-10">12F</h6>
                        </section>
                    </li>
                    <li class="mod-galleryli animate__animated animate__fadeIn wow">
                        <!-- 建議尺寸 -->
                        <a href="galleryin.php" class="mod-galleryli-imgBg">
                            <img src="images/newgallery03.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                        </a>
                        <h3 class="mod-galleryli-tit">
                            <a href="galleryin.php">豐收</a>
                        </h3>
                        <section class="mod-galleryli-info">
                            <h5 class="mb-5">Artist</h5>
                            <h6 class="mb-10">林冠合－威廉氏症候群</h6>
                            <h5 class="mb-5">Material</h5>
                            <h6 class="mb-10">水彩、粉蠟筆</h6>
                            <h5 class="mb-5">Size</h5>
                            <h6 class="mb-10">39.5cm＊27.2cm</h6>
                        </section>
                    </li>
                    <li class="mod-galleryli animate__animated animate__fadeIn wow">
                        <!-- 建議尺寸 -->
                        <a href="galleryin.php" class="mod-galleryli-imgBg">
                            <img src="images/newgallery04.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                        </a>
                        <h3 class="mod-galleryli-tit">
                            <a href="galleryin.php">天空</a>
                        </h3>
                        <section class="mod-galleryli-info">
                            <h5 class="mb-5">Artist</h5>
                            <h6 class="mb-10">許世皇－四肢截肢（口畫家）</h6>
                            <h5 class="mb-5">Material</h5>
                            <h6 class="mb-10">油畫</h6>
                            <h5 class="mb-5">Size</h5>
                            <h6 class="mb-10">50cm＊45cm</h6>
                        </section>
                    </li>
                    <li class="mod-galleryli animate__animated animate__fadeIn wow">
                        <!-- 建議尺寸 -->
                        <a href="galleryin.php" class="mod-galleryli-imgBg">
                            <img src="images/newgallery05.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                        </a>
                        <h3 class="mod-galleryli-tit">
                            <a href="galleryin.php">豐收</a>
                        </h3>
                        <section class="mod-galleryli-info">
                            <h5 class="mb-5">Artist</h5>
                            <h6 class="mb-10">許世皇－四肢截肢（口畫家）</h6>
                            <h5 class="mb-5">Material</h5>
                            <h6 class="mb-10">油畫</h6>
                            <h5 class="mb-5">Size</h5>
                            <h6 class="mb-10">50cm＊45cm</h6>
                        </section>
                    </li>
                    <li class="mod-galleryli animate__animated animate__fadeIn wow">
                        <!-- 建議尺寸 -->
                        <a href="galleryin.php" class="mod-galleryli-imgBg">
                            <img src="images/newgallery06.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                        </a>
                        <h3 class="mod-galleryli-tit">
                            <a href="galleryin.php">家</a>
                        </h3>
                        <section class="mod-galleryli-info">
                            <h5 class="mb-5">Artist</h5>
                            <h6 class="mb-10">許世皇－四肢截肢（口畫家）</h6>
                            <h5 class="mb-5">Material</h5>
                            <h6 class="mb-10">油畫</h6>
                            <h5 class="mb-5">Size</h5>
                            <h6 class="mb-10">50cm＊45cm</h6>
                        </section>
                    </li>
                    <li class="mod-galleryli animate__animated animate__fadeIn wow">
                        <!-- 建議尺寸 -->
                        <a href="galleryin.php" class="mod-galleryli-imgBg">
                            <img src="images/newgallery07.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                        </a>
                        <h3 class="mod-galleryli-tit">
                            <a href="galleryin.php">生命力</a>
                        </h3>
                        <section class="mod-galleryli-info">
                            <h5 class="mb-5">Artist</h5>
                            <h6 class="mb-10">許世皇－四肢截肢（口畫家）</h6>
                            <h5 class="mb-5">Material</h5>
                            <h6 class="mb-10">油畫</h6>
                            <h5 class="mb-5">Size</h5>
                            <h6 class="mb-10">50cm＊45cm</h6>
                        </section>
                    </li>
                    <li class="mod-galleryli animate__animated animate__fadeIn wow">
                        <!-- 建議尺寸 -->
                        <a href="galleryin.php" class="mod-galleryli-imgBg">
                            <img src="images/newgallery08.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                        </a>
                        <h3 class="mod-galleryli-tit">
                            <a href="galleryin.php">五色鳥</a>
                        </h3>
                        <section class="mod-galleryli-info">
                            <h5 class="mb-5">Artist</h5>
                            <h6 class="mb-10">許世皇－四肢截肢（口畫家）</h6>
                            <h5 class="mb-5">Material</h5>
                            <h6 class="mb-10">油畫</h6>
                            <h5 class="mb-5">Size</h5>
                            <h6 class="mb-10">50cm＊45cm</h6>
                        </section>
                    </li>
                    <li class="mod-galleryli animate__animated animate__fadeIn wow">
                        <!-- 建議尺寸 -->
                        <a href="galleryin.php" class="mod-galleryli-imgBg">
                            <img src="images/newgallery01.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                        </a>
                        <h3 class="mod-galleryli-tit">
                            <a href="galleryin.php">星星小丑</a>
                        </h3>
                        <section class="mod-galleryli-info">
                            <h5 class="mb-5">Artist</h5>
                            <h6 class="mb-10">林依柔－唐氏症</h6>
                            <h5 class="mb-5">Material</h5>
                            <h6 class="mb-10">簽字筆</h6>
                            <h5 class="mb-5">Size</h5>
                            <h6 class="mb-10">130cm＊100cm</h6>
                        </section>
                    </li>
                    <li class="mod-galleryli animate__animated animate__fadeIn wow">
                        <!-- 建議尺寸 -->
                        <a href="galleryin.php" class="mod-galleryli-imgBg">
                            <img src="images/newgallery02.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                        </a>
                        <h3 class="mod-galleryli-tit">
                            <a href="galleryin.php">心田花園</a>
                        </h3>
                        <section class="mod-galleryli-info">
                            <h5 class="mb-5">Artist</h5>
                            <h6 class="mb-10">謝建鋒－自閉症與馬凡氏症</h6>
                            <h5 class="mb-5">Material</h5>
                            <h6 class="mb-10">油畫</h6>
                            <h5 class="mb-5">Size</h5>
                            <h6 class="mb-10">12F</h6>
                        </section>
                    </li>
                    <li class="mod-galleryli animate__animated animate__fadeIn wow">
                        <!-- 建議尺寸 -->
                        <a href="galleryin.php" class="mod-galleryli-imgBg">
                            <img src="images/newgallery03.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                        </a>
                        <h3 class="mod-galleryli-tit">
                            <a href="galleryin.php">豐收</a>
                        </h3>
                        <section class="mod-galleryli-info">
                            <h5 class="mb-5">Artist</h5>
                            <h6 class="mb-10">林冠合－威廉氏症候群</h6>
                            <h5 class="mb-5">Material</h5>
                            <h6 class="mb-10">水彩、粉蠟筆</h6>
                            <h5 class="mb-5">Size</h5>
                            <h6 class="mb-10">39.5cm＊27.2cm</h6>
                        </section>
                    </li>
                    <li class="mod-galleryli animate__animated animate__fadeIn wow">
                        <!-- 建議尺寸 -->
                        <a href="galleryin.php" class="mod-galleryli-imgBg">
                            <img src="images/newgallery04.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                        </a>
                        <h3 class="mod-galleryli-tit">
                            <a href="galleryin.php">天空</a>
                        </h3>
                        <section class="mod-galleryli-info">
                            <h5 class="mb-5">Artist</h5>
                            <h6 class="mb-10">許世皇－四肢截肢（口畫家）</h6>
                            <h5 class="mb-5">Material</h5>
                            <h6 class="mb-10">油畫</h6>
                            <h5 class="mb-5">Size</h5>
                            <h6 class="mb-10">50cm＊45cm</h6>
                        </section>
                    </li>
                </ul>
            </div>
        </div>
        <!-- 分頁區 -->
        <div class="eleSelPageBk pagGalliPageBk">
            <button class="eleSelPageArrow eleSelPageArrow--left "></button>
            <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount eleSelPageCount--pageIn mlr-3">1</a>
            <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-3">2</a>
            <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-3">3</a>
            <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-3">4</a>
            <button class="eleSelPageArrow eleSelPageArrow--right"></button>
        </div>

    
    </div>
    
    <?php require('newfooter.php') ?>
        

</body>
</html>

     