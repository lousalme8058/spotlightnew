<!DOCTYPE html>	
<head>
<title>小光點藝廊</title>
<!-- 社群連結fb/line -->
<meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="小光點畫廊 Spotlight gallery" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<!-- 抓創作者 -->
<meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="1010" />
<meta property="og:image:height" content="1010" />

<?php require('newhead.php') ?>

<!-- JSON-LD 結構化資料 --巢狀結構 BlogPosting + video -->
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "BlogPosting",
    //文章標題
    "headline": "Article headline",
    //創作者照片
    "image": [
        "https://example.com/photos/1x1/photo.jpg",
        "https://example.com/photos/4x3/photo.jpg",
        "https://example.com/photos/16x9/photo.jpg"
    ],
    "video": {
        "@type": "VideoObject",
        //影片名稱
        "name": "Cat video",
        //上傳日期
        "uploadDate": "2019-07-19",
        //影片封面縮圖
        "thumbnailUrl": "http://www.example.com/cat.jpg",
        //描述
        "description": "Watch this cat jump over a fence!",
        //影片實際空間網址
        "contentUrl": "http://www.example.com/cat_video_full.mp4"
    }
}
</script>

<!-- 首頁輪播 -->
<link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<!-- <link rel="stylesheet" href="vendor/Owl/owl.theme.default.css"> -->
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    $('.owl-custom01').owlCarousel({
        animateOut: 'fadeOut',
        animateIn: 'flipInX',
        loop: false,
        margin: 0,
        stagePadding: 0,
        smartSpeed: 450,
        dots: true,
        // center: true,
        // nav:  true,
        responsive: {
            320: {
                items: 1
            },
            768:{
                items: 2,
                // margin: 25,
            },
            1024:{
                items: 3
            },
            1800:{
                items: 4
            },
        }
    });
});
// 動畫效果
$(document).ready(function() {

});
$(window).on('load',function(){
    let contentH = $(".js-pagCreinArticleBk").offset().top - 250;
    // console.log(contentH);

    $(window).scroll(function(){
        // console.log( $(window).scrollTop());
        if($(window).scrollTop() >= contentH){
            TweenMax.to(".js-Circle01", 0.5, {delay: 0.7,scaleX:"1", scaleY:"1", opacity:"1" ,ease: Power4.inOut });
            TweenMax.to(".js-Circle02", 0.5, {delay: 1,scaleX:"1", scaleY:"1", opacity:"1" ,ease: Power4.inOut });
            // console.log("條件成立");
        };
    });
});
</script>
<body>
    <!-- loading動畫 -->
    <div class="js-patLoadingAniBg patLoading">
    </div>

    <?php require('newheader.php') ?>

    <div class="pagCreinBk">
        <article class="pagCreinMainBk">
            <!-- 創作者照片 -->
            <div class="pagCreinImgBk">
                <img src="images/newcreator01.png" alt="創作者照片" class="pagCreinImgBk-img">
            </div>
            <!-- slogan -->
            <div class="patSection pagCreinSloganBk">
                <div class="pagCreinSloganBk-block pagCreinSloganBk-dec">,,</div>
                <h1 class="pagCreinSloganBk-block pagCreinSloganBk-slogan">為什麼一定要一樣</h1>
                <h6 class="pagCreinSloganBk-block">陳沛儒 － 腦性麻痺</h6>
                <div class="pagCreinSloganBk-block pagCreinSloganBk-link">
                    <a href="javascript:void(0);" class="btnLinkBt" title="前往畫家網站">
                        前往畫家網站
                    </a>
                </div>
            </div>
        </article>
        <div class="js-pagCreinArticleBk pagCreinArticleBk">
            <div class="patSection pagCreinArticle-sloganBk">
                <img src="images/newink01.png" alt="circle" class="js-Circle01 pagCreinArticleCircle01 ">
                <img src="images/newink02.png" alt="circle" class="js-Circle02 pagCreinArticleCircle02">
                <!-- title -->
                <div class="modAsideAside-titArea typo-textAlignCenter marginAuto">
                    <h2 class="modLetterAniArea">
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">T</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">h</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">i</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">s</span>
                        <span class="modLetterAniArea-blank"></span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">i</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">s</span>
                        <span class="modLetterAniArea-blank"></span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">m</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">y</span>
                        <span class="modLetterAniArea-blank"></span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">s</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">t</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">o</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">r</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">y</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow"> .</span>
                    </h2>
                    <h2 class="typo-chTit mt-5 modLetterAniArea">
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">－</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">我</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">很</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">與</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">眾</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">不</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">同</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">。</span>
                    </h2>
                </div>
            </div>
            
            <!-- 文編區 -->
            <div class="patSection pagCreinArticle">
                <!-- 文編放這邊 -->
                <div class="textBk">
                    <!-- <section class="textArticleParagraph">
                        在一個無憂無慮充滿愛的環境下長大，只要看過我的人一定會記得我，因為「輪椅」代替我的雙腳陪伴我長大，是的，我很與眾不同，因為我是一位腦性
                        麻痺的女孩。
                        <br />
                        <br />
                        我的爸爸，是直升機飛行員，他說他把我當正常的孩子在養，因此一直以來「殘障」這個名詞我也不為然，反倒是我從小到大最常去的地方就是醫院，
                        並和醫生及老師們成為了好朋友，是不是很有趣又好玩呢！有一部電影「看見台灣」，我的爸爸是幕後的飛行英雄，但是非常不幸104年11月意外墜機
                        ，我和家人隨著爸爸的死，我們的心也埋葬在台灣了，隨之而來「我」卻成為了眾人關懷憐憫的焦點，「殘障」這2個字突然又被放大了，望著媽媽悲
                        傷的背影，我覺得快樂和陽光漸漸地遠離我，軟弱找上門了，讓我不知如何是好･･･為了不讓媽媽擔心，也怕媽媽太辛苦，有一年的時間，身邊的親友
                        教我做泡菜，也教我做餅乾，填鴨似的給我目標，讓我很忙碌，但是我知道內心裡像破了一個洞，怎麼樣都無法填埔，我覺得似乎找不回那一個勇敢開
                        朗的自己了，讓我很煩惱。
                    </section>
                    <h3 class="textArticleTit">思念帶領我走向未來</h3>
                    <section class="textArticleParagraph">
                        過了沒多久，突然有天夜裡，夢見爸爸拿著蓮花來看我，夢境裡非常寧靜舒服，果然沒幾天就遇到油畫的啟蒙老師，讓我一頭栽進畫畫的世界，原來爸
                        爸一直都永遠的陪伴在我的身邊，因為太想念、太思念、太懷念･･･想要用畫筆展現，告訴在天國的爸爸，今後，我會勇敢，並重拾快樂的心，讓每
                        個明天都會更好･･･最後，在這個世界上，謝謝我的家人，陪我一起勇敢。
                    </section>
                    <img src="images/newText01.png" alt="畫家陳姵儒與她在小光點第一幅售出的作品 －「白雲下的直升機」" class="textImg">
                    <p class="textImgNote">畫家陳姵儒與她在小光點第一幅售出的作品 －「白雲下的直升機」</p> -->

                    <section class="textArticleParagraph">
                        文字內容
                    </section>
                    <h3 class="textArticleTit">
                        段落標題
                    </h3>
                    <section class="textArticleParagraph">
                        文字內容
                    </section>
                    <img src="images/newText01.png" alt="圖片" class="textImg">
                    <p class="textImgNote">圖片備註</p>
                </div>
            </div>        
        </div>

        <!-- 履歷區跟影片區 -->
        <div class="pagCreinResumeVideoBk">
            <!-- 履歷區 -->
            <div class="patSection pagCreinResumeBk">
                <div class="mod-decTitArea">
                    <h4 class="mod-decTitArea--tit mt-20">Intro</h4>
                    <div class="mod-decTitArea--content overflow-hidden">
                        <h2 class="pagCreinResumeBk-titBk">Resume</h2>
                        <section class="pagCreinResumeBk-nameInfoBk">
                            <h5 class="mb-5">Artist</h5>
                            <h6 class="mb-15 pr-20">鄭美珠 － 輕度魚鱗癬與顱顏併發症</h6>
                        </section>
                        <section class="pagCreinResumeBk-nameInfoBk">
                            <h5 class="mb-5">Birth</h5>
                            <h6 class="mb-15 pr-20">1995年出生</h6>
                        </section>
                    </div>
                </div>
                <div class="mod-decTitArea pagCreinResumeBk-experBk">
                    <h4 class="mod-decTitArea--tit mt-5">Experience</h4>
                    <article class="mod-decTitArea--content overflow-hidden">
                        <p class="">
                            ✧　2020　第九屆特殊美學【我畫我畫】繪畫比賽全國優等<br />
                            ✧　2020　第十六屆國際身心障礙者藝術巔峰創作聯展<br />    
                            ✧　2019　第九屆全國心智障礙者才藝大賽 平面繪畫成人組全國特優<br />    
                            ✧　2019　Business ART Fair FIAC國際當代藝術週<br />    
                            ✧　2019　法國巴黎國際藝術沙龍博覽會（羅浮宮卡爾塞廳）<br />
                            ✧　2020　第九屆特殊美學【我畫我畫】繪畫比賽全國優等<br />
                            ✧　2020　第十六屆國際身心障礙者藝術巔峰創作聯展<br />    
                            ✧　2019　第九屆全國心智障礙者才藝大賽 平面繪畫成人組全國特優<br />    
                            ✧　2019　Business ART Fair FIAC國際當代藝術週<br />        
                        </p>
                        <p class="">
                            ✧　澎湖縣美術協會與澎湖縣國際藝術交流協會理事長<br />
                            ✧　2021　《安地斯山的母親》繪本 周大觀基金會出版<br />
                            ✧　1992-2020　澎湖、金門與台灣各縣市個人畫展50場<br />
                            ✧　2017　《白鳥美珠童年記事》白鳥美珠美術館出版<br />
                            ✧　2010　總統教育獎<br />
                            ✧　1998　教育部第15屆全國美展油畫入選<br />    
                        </p>
                    </article>
                </div>
            </div>
            <!-- 影片區 -->
            <article class="pagCreinvideoBk">
                <a href="https://www.youtube.com/watch?v=ac_XUj048I0&ab_channel=Muse%E9%97%94%E5%AE%B6%E6%AD%A1" class="pagCreinvideoBk-playBt" target="_blank">
                    <img src="images/newvideo-bt.svg" alt="play video button" class="pagCreinvideoBk-playBt--icon">
                </a>
                <div class="pagCreinvideoBk-imgBk">
                    <!-- 建議尺寸 960 * auto -->
                    <img src="images/newvideoBg.png" alt="影片封面" class="pagCreinvideoBk-imgBk--img">
                </div>
            </article>
        </div>
    </div>

    <!-- 畫作區 -->
    <div class="pagCreingGalleryBk">
        <div class="mod-decTitArea pagCreingGalleryBk-titBk">
            <h4 class="mod-decTitArea--tit mt-20">Painting</h4>
            <div class="mod-decTitArea--content pagCreingGalleryBk-bigTit">
                <h2 class="pagCreinResumeBk-titBk">Gallery</h2>
            </div>
        </div>
        <div class="owl-custom01 owl-carousel owl-theme">
            <li class="mod-galleryli pagCreingGalleryBk-galleryBk">
                <!-- 建議尺寸 -->
                <a href="galleryin.php" class="mod-galleryli-imgBg">
                    <img src="images/newgallery01.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                </a>
                <span class="mod-galleryli-label">已售出</span>
                <h3 class="mod-galleryli-tit">
                    <a href="galleryin.php">星星小丑</a>
                </h3>
                <section class="mod-galleryli-info">
                    <h5 class="mb-5">Artist</h5>
                    <h6 class="mb-10">陳沛儒 － 腦性麻痺</h6>
                    <h5 class="mb-5">Material</h5>
                    <h6 class="mb-10">簽字筆</h6>
                    <h5 class="mb-5">Size</h5>
                    <h6 class="mb-10">130cm＊100cm</h6>
                </section>
            </li>

            <li class="mod-galleryli pagCreingGalleryBk-galleryBk">
                <!-- 建議尺寸 -->
                <a href="galleryin.php" class="mod-galleryli-imgBg">
                    <img src="images/newgallery03.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                </a>
                <h3 class="mod-galleryli-tit">
                    <a href="galleryin.php">豐收</a>
                </h3>
                <section class="mod-galleryli-info">
                    <h5 class="mb-5">Artist</h5>
                    <h6 class="mb-10">陳沛儒 － 腦性麻痺</h6>
                    <h5 class="mb-5">Material</h5>
                    <h6 class="mb-10">水彩、粉蠟筆</h6>
                    <h5 class="mb-5">Size</h5>
                    <h6 class="mb-10">39.5cm＊27.2cm</h6>
                </section>
            </li>
            <li class="mod-galleryli pagCreingGalleryBk-galleryBk">
                <!-- 建議尺寸 -->
                <a href="galleryin.php" class="mod-galleryli-imgBg">
                    <img src="images/newgallery06.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                </a>
                <h3 class="mod-galleryli-tit">
                    <a href="galleryin.php">家</a>
                </h3>
                <section class="mod-galleryli-info">
                    <h5 class="mb-5">Artist</h5>
                    <h6 class="mb-10">陳沛儒 － 腦性麻痺</h6>
                    <h5 class="mb-5">Material</h5>
                    <h6 class="mb-10">水彩、粉蠟筆</h6>
                    <h5 class="mb-5">Size</h5>
                    <h6 class="mb-10">39.5cm＊27.2cm</h6>
                </section>
            </li>
        </div>
    </div>
    
    <?php require('newfooter.php') ?>
        

</body>
</html>
