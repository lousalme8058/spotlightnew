<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="content-language" content="zh-TW" />
<meta name="viewport" content="width=device-width , initial-scale=1" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="author"  content="小宇宙資訊有限公司,www.cosmosdesign.tw" />
<meta name="COPYRIGHT" content="Copyright (c) by 小宇宙資訊有限公司,www.cosmosdesign.tw">
<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<meta name="keywords" Lang="EN" content=" "/>
<meta name="keywords" Lang="zh-TW" content=" " />
<!--css檔連結區-->
<link href="css/cssreset.css" rel="stylesheet" type="text/css" media="all" />
<!-- 網站區layout -->
<link href="css/layoutnew.css" rel="stylesheet" type="text/css" media="all" />

<!--js檔連結區-->
<script src="js/jquery-3.4.1.js"></script>
<!--載入tweenmax-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.4/plugins/ScrollToPlugin.min.js"></script>
<script src="js/CustomEase.min.js"></script>

<!-- js檔 -->
<script src="js/web-js.js"></script>


<!--google 雲端字型-->
<link rel="preconnect" href="https://fonts.gstatic.com">
<!-- Lateef 字型 -->
<!-- font-family: 'Lateef', cursive; -->
<link href="https://fonts.googleapis.com/css2?family=Lateef&display=swap" rel="stylesheet">
<!-- Montserrat 字型 -->
<!-- font-family: 'Montserrat', sans-serif; -->
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600;700&display=swap" rel="stylesheet">
<!-- noto 思源宋體 -->
<link href="https://fonts.googleapis.com/css2?family=Noto+Serif+TC:wght@500;600;700&display=swap" rel="stylesheet">
<!-- noto 思源黑體 -->
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@400;500&display=swap" rel="stylesheet">


<!-- wow.js -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
<script src="js/wow.js"></script>
<script>
    var wow = new WOW(
    {
      boxClass: 'wow', // 要套用WOW.js縮需要的動畫class(預設是wow)
      animateClass: 'animated', // 要"動起來"的動畫(預設是animated, 因此如果你有其他動畫library要使用也可以在這裡調整)
      offset: 0, // 距離顯示多遠開始顯示動畫 (預設是0, 因此捲動到顯示時才出現動畫)
      mobile: true, // 手機上是否要套用動畫 (預設是true)
      live: true, // 非同步產生的內容是否也要套用 (預設是true, 非常適合搭配SPA)
      callback: function(box) {
      // 當每個要開始時, 呼叫這裡面的內容, 參數是要開始進行動畫特效的element DOM
    },
      scrollContainer: null // 可以設定成只套用在某個container中捲動才呈現, 不設定就是整個視窗
    }
    );
    wow.init();
</script>

<!-- animate -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>




<!-- 社群連結fb/line -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="小光點畫廊 Spotlight gallery" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="" />
<meta property="og:image:height" content="" />
 -->




