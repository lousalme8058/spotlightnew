<!DOCTYPE html>	
<head>
<title>小光點藝廊</title>

<!-- 社群連結fb/line -->
<meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="小光點畫廊 Spotlight gallery" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<!-- 抓banner圖 -->
<meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" />
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<meta property="og:image:width" content="1010" />
<meta property="og:image:height" content="1010" />

<?php require('newhead.php') ?>
<!-- JSON-LD 結構化資料 -- Store -->
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "Store",
    "@id": "http://www.spotlight-gallery.com.tw/",
    "image": [
        "http://www.spotlight-gallery.com.tw/images/newvideoBg.png"
    ],
    "logo": "http://www.spotlight-gallery.com.tw/images/logo.png",
    "url": "http://www.spotlight-gallery.com.tw/",
    "address": {
        "@type": "PostalAddress",
        "streetAddress": "新豐里183號",
        "addressLocality": "西螺鎮",
        "addressRegion": "雲林縣",
        "postalCode": "648",
        "addressCountry": "Taiwan"
    },
    //名稱
    "name": "Middle of Nowhere Foods",
    //電話
    "telephone": " 0906-956-005",
    //營業時間
    "openingHours": "Mo,Tu,We,Th,Fr,Sa,Su 09:00-21:00",
    "openingHoursSpecification":
    [
        {
        "@type": "OpeningHoursSpecification",
        "opens": "09:00:00",
        "closes": "21:00:00"
        }
    ]
}
</script>

<!-- 首頁輪播 -->
<link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<!-- <link rel="stylesheet" href="vendor/Owl/owl.theme.default.css"> -->
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
$(document).ready(function() {
    $('.owl-custom01').owlCarousel({
        animateOut: 'fadeOut',
        animateIn: 'flipInX',
        loop: true,
        margin:0,
        stagePadding:0,
        smartSpeed:450,
        dots: true,
        // nav:  true,
        responsive: {
            320: {
                items: 1
            },
        }
    });
    $('.owl-custom02').owlCarousel({
        animateOut: 'fadeOut',
        animateIn: 'flipInX',
        loop: false,
        margin:0,
        stagePadding:0,
        smartSpeed:450,
        dots: true,
        // nav:  true,
        responsive: {
            320: {
                items: 1
            },
            1024: {
                items: 2
            },
        }
    });
});
// 動畫效果
$(window).on('load',function(){
    let positionY = $(".indGalleryArea").offset().top;
    // console.log(positionY);
    let exhiPositionY = $(".js-indExhiY").offset().top - 500;
    let exhiPositionYHide = $(".js-indExhiY").offset().top + 780;
    // console.log(exhiPositionY);
    // console.log(exhiPositionYHide);
    $(window).scroll(function(){
        if($(window).width() > 768 && $(window).scrollTop() >= positionY){
            $(".js-loadingCircle03").css("top", "-230px");
        }else{
            $(".js-loadingCircle03").css("top", "270px");
        };
        if($(window).width() > 768 && $(window).scrollTop() >= exhiPositionY && $(window).scrollTop() <= exhiPositionYHide){
            TweenMax.to(".js-indCircle01", 0.3, {delay: 0.2, scaleX:"1", scaleY:"1", opacity:"1" ,ease: Power4.inOut });
            // console.log("條件成立01");
        }else{
            TweenMax.to(".js-indCircle01", 0.3, {delay: 0.2, scaleX:"0", scaleY:"0", opacity:"0" ,ease: Power4.inOut });
            // console.log("條件成立02");
        };
    });
});
</script>
<body>
    <!-- loading動畫 -->
    <div class="js-patLoadingAniBg patLoading">
        <img src="images/newink03.png" alt="" class="patLoading-color01 js-loadingCircle01">
        <img src="images/newink02.png" alt="" class="patLoading-color02 js-loadingCircle02">
    </div>
    <img src="images/newink01.png" alt="" class="patLoading-color03 js-loadingCircle03">
    <img src="images/newink04.png" alt="" class="indColorCircle js-indCircle01">

    <!-- <picture>
        <source srcset="images/banner01-portrait.png" media="(max-width: 48em)" width="100%" height="auto">
        <source srcset="images/banner01-landscape.png" media="(min-width: 48em)" width="100%" height="auto">
        <img src="images/banner01.png" alt="預設輪播圖" width="100%" height="auto">    
    </picture> -->

    <?php require('newheader.php') ?>


    <!-- 活動banner區 -->
    <article class="indBanner">
        <a href="actli.php" class="indBanner-imgBk js-indexBanner" title="畫廊活動">
            <!-- 建議尺寸 1010*1010 -->
            <img src="images/newbanner01.png" alt="預設輪播圖" width="100%" height="auto" class="indBanner-imgBk--img js-indexBanner-img">    
        </a>
        <section class="patSection indBanner-titArea js-indexBanner-info">
            <div class="mod-decTitArea">
                <h4 class="mod-decTitArea--tit">Title</h4>
                <h1 class="indBanner-titArea--tit mod-decTitArea--content">
                    <a href="actli.php" title="活動內容">那道光 — 小光點畫廊 ✕ 陶緣彩瓷聯名畫展</a>
                </h1>
            </div>
            <div class="mod-decTitArea">
                <h4 class="mod-decTitArea--tit">Info</h4>
                <div class="mod-decTitArea--content">
                    <h5 class="mb-5">Date</h5>
                    <h6 class="mb-15">2020.07.20 - 08.30</h6>
                    <h5 class="mb-5">Classify</h5>
                    <h6>畫廊活動</h6>
                </div>
            </div>
            <div class="indBanner-titArea--btArea">
                <a href="actli.php" class="btnMorebt" title="畫廊活動">
                    <span class="btnMorebt-circle"></span>
                    <span class="btnMorebt-word">READ MORE</span>
                    <img src="images/newarrow-right.svg" alt="arrow" class="btnMorebt-arrow">
                </a>
            </div>
        </section>
        <div class="clear"></div>
    </article>


    <!-- 網站介紹&精選畫作區 -->
    <div class="indGalleryArea">
        <!-- 網站介紹區 -->
        <div class="patSection patSectionWidth">
            <article class="indGalleryIntro">
                <div class="indGalleryIntro-titArea">
                    <h2 class="modLetterAniArea">
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">S</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">p</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">o</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">t</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">l</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">i</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">g</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">h</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">t</span>
                        <span class="modLetterAniArea-blank"></span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">G</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">a</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">l</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">l</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">e</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">r</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">y</span>
                    </h2>
                    <h2 class="typo-chTit mt-5 modLetterAniArea">
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">看</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">見</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">身</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">心</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">障</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">礙</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">藝</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">術</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">。</span>
                    </h2>
                </div>
                <div class="indGalleryIntro-emText">
                    <a href="galleryli.php" class="btnGalleryIntro-emText--text wow">
                        你是這世界的溫暖光點<br />讓我們牽起手照亮世界
                    </a>
                    <div class="indGalleryIntro-emText--bt">
                        <a href="galleryli.php" class="btnEmbt" title="進入小光點線上畫廊">
                            <img src="images/newbig-bt-circle.svg" alt="circle" class="btnEmbt-circle wow">
                            <img src="images/newarrow-right.svg" alt="arrow" class="btnEmbt-arrow wow">
                        </a>
                    </div>
                </div>
                <p class="indGalleryIntro-text wow">
                    人生能為一件事情專注，是多麼的美好。
                    <br /><br />
                    身心障礙的鬥士們，在生活中會遇到比別人更多的阻礙與艱難，經濟上的壓力讓身心焦慮煎熬，日漸虛耗的身體讓工作機會越來越少。
                    <br /><br />
                    在茫然中，他們透過繪畫慢慢找回自己和面對生活的勇氣。
                </p>
                <div class="indProInfoArea--bt">
                
                </div>
            </article>
        </div>
        <!-- 精選畫作區 -->
        <div class="patSection patSectionWidth indGalleryListarea">
            <ul class="indGalleryListarea-column">
                <!-- 一個畫作 -->
                <li class="indGalleryLi mod-galleryli animate__animated animate__fadeIn wow">
                    <!-- 建議尺寸 1440* auto -->
                    <!-- 所有畫作圖都用同一張圖即可 -->
                    <a href="galleryin.php" class="mod-galleryli-imgBg">
                        <img src="images/newgallery01.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                    </a>
                    <h3 class="mod-galleryli-tit">
                        <a href="galleryin.php">星星小丑</a>
                    </h3>
                    <section class="mod-galleryli-info">
                        <h5 class="mb-5">Artist</h5>
                        <h6 class="mb-10">林依柔－唐氏症</h6>
                        <h5 class="mb-5">Material</h5>
                        <h6 class="mb-10">簽字筆</h6>
                        <h5 class="mb-5">Size</h5>
                        <h6 class="mb-10">130cm＊100cm</h6>
                    </section>
                </li>
                <li class="indGalleryLi mod-galleryli animate__animated animate__fadeIn wow">
                    <!-- 建議尺寸 -->
                    <!-- 建議尺寸 1440* auto -->
                    <a href="galleryin.php" class="mod-galleryli-imgBg">
                        <img src="images/newgallery02.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                    </a>
                    <h3 class="mod-galleryli-tit">
                        <a href="galleryin.php">心田花園</a>
                    </h3>
                    <section class="mod-galleryli-info">
                        <h5 class="mb-5">Artist</h5>
                        <h6 class="mb-10">謝建鋒－自閉症與馬凡氏症</h6>
                        <h5 class="mb-5">Material</h5>
                        <h6 class="mb-10">油畫</h6>
                        <h5 class="mb-5">Size</h5>
                        <h6 class="mb-10">12F</h6>
                    </section>
                </li>
                <li class="indGalleryLi mod-galleryli animate__animated animate__fadeIn wow">
                    <!-- 建議尺寸 -->
                    <!-- 建議尺寸 1440* auto -->
                    <a href="galleryin.php" class="mod-galleryli-imgBg">
                        <img src="images/newgallery03.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                    </a>
                    <h3 class="mod-galleryli-tit">
                        <a href="galleryin.php">豐收</a>
                    </h3>
                    <section class="mod-galleryli-info">
                        <h5 class="mb-5">Artist</h5>
                        <h6 class="mb-10">林冠合－威廉氏症候群</h6>
                        <h5 class="mb-5">Material</h5>
                        <h6 class="mb-10">水彩、粉蠟筆</h6>
                        <h5 class="mb-5">Size</h5>
                        <h6 class="mb-10">39.5cm＊27.2cm</h6>
                    </section>
                </li>
                <li class="indGalleryLi mod-galleryli animate__animated animate__fadeIn wow">
                    <!-- 建議尺寸 -->
                    <!-- 建議尺寸 1440* auto -->
                    <a href="galleryin.php" class="mod-galleryli-imgBg">
                        <img src="images/newgallery04.png" alt="畫作名稱" class="mod-galleryli-imgBg--img">
                    </a>
                    <h3 class="mod-galleryli-tit">
                        <a href="galleryin.php">天空</a>
                    </h3>
                    <section class="mod-galleryli-info">
                        <h5 class="mb-5">Artist</h5>
                        <h6 class="mb-10">許世皇－四肢截肢（口畫家）</h6>
                        <h5 class="mb-5">Material</h5>
                        <h6 class="mb-10">油畫</h6>
                        <h5 class="mb-5">Size</h5>
                        <h6 class="mb-10">50cm＊45cm</h6>
                    </section>
                </li>

            </ul>
        </div>
    </div>


    <div class="whiteHelfBgArea">
        <!-- <div class="whiteHelfBg"></div> -->

        <!-- 展覽區 顯示4個 -->
        <div class="patSection indExhiArea js-indExhiY">
            <div class="owl-custom01 owl-carousel owl-theme">
                <!-- 一個展覽 -->
                <article class="indExhi">
                    <div class="indExhi-enTit mod-decTitArea">
                        <h4 class="mod-decTitArea--tit mt-25">Title</h4>
                        <div class="mod-decTitArea--content">
                            <h2>Exhibition</h2>
                        </div>
                    </div>
                    <!-- 展覽資訊 -->
                    <div class="indExhi-photoArea mod-decTitArea">
                        <h4 class="mod-decTitArea--tit indExhi-photoArea--year">2020</h4>
                        <h2 class="mod-decTitArea--content indExhi-photoArea--date">07.31</h2>
                        <a href="galleryin.php" class="indExhi-photoArea--photo">
                            <!-- 建議尺寸 750 * 500 -->
                            <img src="images/newact01.png" alt="活動照片" class="" width="100%" height="auto">
                        </a>
                    </div>
                    <div class="indExhi-infoArea mod-decTitArea">
                        <h4 class="mod-decTitArea--tit mt-12">Info</h4>
                        <section class="mod-decTitArea--content">
                            <h3 class="typo-chTit indExhi-infoArea--tit">
                                <a href="actin.php" title="活動內容" class="">「那道光」小光點畫廊 ✕ 陶緣彩瓷聯名畫展</a>
                            </h3>
                            <h5 class="mb-5">Date</h5>
                            <h6 class="mb-15">2020-09-04～2020-11-03</h6>
                            <h5 class="mb-5">Time</h5>
                            <h6 class="mb-15">24小時營業</h6>
                            <h5 class="mb-5">Location</h5>
                            <h6 class="mb-15">家樂福青海店B1手扶梯旁：台中市西屯區青海路二段207-18號</h6>
                        </section>
                </article>
                <!-- 一個展覽 -->
                <article class="indExhi">
                    <div class="indExhi-enTit mod-decTitArea">
                        <h4 class="mod-decTitArea--tit mt-25">Title</h4>
                        <div class="mod-decTitArea--content">
                            <h2>Exhibition</h2>
                        </div>
                    </div>
                    <!-- 展覽資訊 -->
                    <div class="indExhi-photoArea mod-decTitArea">
                        <h4 class="mod-decTitArea--tit indExhi-photoArea--year">2020</h4>
                        <h2 class="mod-decTitArea--content indExhi-photoArea--date">07.31</h2>
                        <a href="galleryin.php" class="indExhi-photoArea--photo">
                            <img src="images/newact01.png" alt="活動照片" class="" width="100%" height="auto">
                        </a>
                    </div>
                    <div class="indExhi-infoArea mod-decTitArea">
                        <h4 class="mod-decTitArea--tit mt-12">Info</h4>
                        <section class="mod-decTitArea--content">
                            <h3 class="typo-chTit indExhi-infoArea--tit">
                                <a href="actin.php" title="活動內容" class="">「那道光」小光點畫廊 ✕ 陶緣彩瓷聯名畫展</a>
                            </h3>
                            <h5 class="mb-5">Date</h5>
                            <h6 class="mb-15">2020-09-04～2020-11-03</h6>
                            <h5 class="mb-5">Time</h5>
                            <h6 class="mb-15">24小時營業</h6>
                            <h5 class="mb-5">Location</h5>
                            <h6 class="mb-15">家樂福青海店B1手扶梯旁：台中市西屯區青海路二段207-18號</h6>
                        </section>
                </article>
            </div>
        </div>


        <!-- 周邊商品區 -->
        <div class="patSection indProArea">
            <!-- 文字說明區 -->
            <article class="indProInfoArea">
                <h2 class="indProInfoArea--enTit">Products</h2>
                <h3 class="indProInfoArea--chTit">周邊商品</h3>
                <p class="indProInfoArea--text">
                    小光點畫廊與各企業合作所推出聯名商品，將部分收入運用於小光點教室，讓身心障礙孩子能有個安心快樂的繪畫教室。
                    <br /><br />
                    購買周邊商品，支持身心障礙藝術前行，讓您的生活增添美感帶來的好心情！
                </p>
                <div class="indProInfoArea--btArea">
                    <a href="proli.php" class="btnMorebt" title="畫廊活動">
                        <span class="btnMorebt-circle"></span>
                        <span class="btnMorebt-word">READ MORE</span>
                        <img src="images/newarrow-right.svg" alt="arrow" class="btnMorebt-arrow">
                    </a>
                </div>
            </article>
            <!-- 周邊商品 顯示2個 -->
            <div class="indProListBk">
                <ul class="owl-custom02 owl-carousel owl-theme">
                    <!-- 一個商品 -->
                    <li class="indProList">
                        <a href="proin.php" title="周邊商品連結" class="indProList-imgbk">
                            <!-- 建議尺寸 420 * 420 -->
                            <img src="images/newpro01.png" alt="周邊商品圖片" class="indProList-imgbk--img">
                        </a>
                        <h3 class="indProList-tit"><a href="proin.php">8吋方瓷盤―湖心亭│附盤架</a></h3>
                        <div class="indProList-priceArea">
                            <h5 class="indProList-priceArea--unit mb-8">一個</h5>
                            <h6 class="indProList-priceArea--price">$ 890</h6>
                        </div>
                    </li>
                    <!-- 一個商品 -->
                    <li class="indProList">
                        <a href="proin.php" title="周邊商品連結" class="indProList-imgbk">
                            <!-- 建議尺寸 420 * 420 -->
                            <img src="images/newpro02.png" alt="周邊商品圖片" class="indProList-imgbk--img">
                        </a>
                        <h3 class="indProList-tit"><a href="proin.php">小光點畫家明信片組</a></h3>
                        <div class="indProList-priceArea">
                            <h5 class="indProList-priceArea--unit mb-8">一組</h5>
                            <h6 class="indProList-priceArea--price">$ 120</h6>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
 
    
    <?php require('newfooter.php') ?>
        

</body>
</html>

     