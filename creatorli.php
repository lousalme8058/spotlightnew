<!DOCTYPE html>	
<head>
<title>小光點藝廊</title>
<!-- 社群連結fb/line -->
<meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="小光點畫廊 Spotlight gallery" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="" />
<meta property="og:image:height" content="" />

<?php require('newhead.php') ?>
<script language="javascript">
// 動畫效果
$(window).on('load',function(){
    let contentH = $(".pagGalliAside").height() - 1000;
    console.log(contentH);
    $(window).scroll(function(){
        if($(window).width() > 768 && $(window).scrollTop() <= contentH){
            TweenMax.to(".js-Circle01", 0.5, {delay: 0.5,scaleX:"1", scaleY:"1", opacity:"1" ,ease: Power4.inOut });
            // console.log("條件成立");
        }else if($(window).scrollTop() >= contentH){
            TweenMax.to(".js-Circle01", 0.3, {delay: 0.5,scaleX:"0", scaleY:"0", opacity:"0" ,ease: Power4.inOut });
        }
    });
});
</script>
<body>
    <!-- loading動畫 -->
    <!-- <div class="js-patLoadingAniBg patLoading">
    </div> -->
    <img src="images/newink01.png" alt="" class="pagCreliColorCircle js-Circle01 wow">

    <?php require('newheader.php') ?>

    <div class="whiteHelfBgArea">

        <!-- 畫廊列表 -->
        <div class="patSection pagCreliBk">
            <!-- 側邊欄位區 -->
            <aside class="modAside pagGalliAside">
                <!-- 麵包屑 -->
                <article class="eleBite mb-15">
                    <a href="index.php" class="eleBite-link">
                        首頁
                    </a>
                    <a href="creatorli.php" class="eleBite-link">
                        創作者介紹
                    </a>
                </article>
                <!-- title -->
                <div class="modAsideAside-titArea">
                    <h2 class="modLetterAniArea">
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">S</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">p</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">o</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">t</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">l</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">i</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">g</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">h</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">t</span>
                        <span class="modLetterAniArea-blank"></span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">C</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">r</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">e</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">a</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">t</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">o</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">r</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--en wow">s</span>
                    </h2>
                    <h1 class="typo-chTit mt-5 modLetterAniArea">
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">創</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">作</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">者</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">介</span>
                        <span class="modLetterAniArea-letter modLetterAniArea-letter--ch wow">紹</span>
                    </h1>
                </div>
                <p class="pagCreliAside-text">
                    小光點畫廊的宗旨是推廣身心障礙者不再被貼上「需要幫助」的標籤，而是能靠著自己的力量為生命奮鬥，讓我們一起跨越那條無形的界線，體驗生命真正的價值，你是這世界的溫暖光點，讓我們牽起手照亮世界！
                    <br />
                    <br />
                    身心障礙者的作品需要被更多人看見，更需要您的鼓勵與肯定，您的購畫能為他們開啟一道扭轉人生的大門，因為您的支持讓他們看見希望！
                </p>   
            </aside>

            <!-- 創作者列表 一頁10個 -->
            <ul class="pagCreli">
                <!-- 一個創作者 -->
                <li class="pagCrelist">
                    <a href="creatorin.php" class="pagCrelist-textBk plr-15 animate__animated animate__fadeIn wow">
                        <div class="pagCrelist-textBk--dec">,,</div>
                        <h3 class="pagCrelist-textBk--tit mt-25 mb-15 plr-20">人生中有些路你不得不走，而且一旦踏出，就再也不能回頭</h3>
                        <h6 class="plr-30">陳沛儒 － 腦性麻痺</h6>
                    </a>
                    <a href="creatorin.php" class="pagCrelist-imgBk  animate__animated animate__fadeIn wow" title="查看創作者介紹">
                        <!-- 建議尺寸 1010 * 1010 -->
                        <img src="images/newcreator01.png" alt="創作者照片" class="pagCrelist-imgBk--img">
                    </a>
                </li>
                <li class="pagCrelist">
                    <a href="creatorin.php" class="pagCrelist-textBk plr-15 animate__animated animate__fadeIn wow">
                        <div class="pagCrelist-textBk--dec">,,</div>
                        <h3 class="pagCrelist-textBk--tit mt-25 mb-15 plr-20">有才華的人，沒有資格自卑，要勇於跟世界分享才華</h3>
                        <h6 class="plr-30">鄭美珠 － 輕度魚鱗癬與顱顏併發症</h6>
                    </a>
                    <a href="creatorin.php" class="pagCrelist-imgBk  animate__animated animate__fadeIn wow" title="查看創作者介紹">
                        <!-- 建議尺寸 1010 * 1010 -->
                        <img src="images/newcreator01.png" alt="創作者照片" class="pagCrelist-imgBk--img">
                    </a>
                </li>
                <li class="pagCrelist">
                    <a href="creatorin.php" class="pagCrelist-textBk plr-15 animate__animated animate__fadeIn wow">
                        <div class="pagCrelist-textBk--dec">,,</div>
                        <h3 class="pagCrelist-textBk--tit mt-25 mb-15 plr-20">在最深的絕望裡，遇見最美麗的風景</h3>
                        <h6 class="plr-30">馮嘉嫻(口畫) － 脊髓損傷者</h6>
                    </a>
                    <a href="creatorin.php" class="pagCrelist-imgBk  animate__animated animate__fadeIn wow" title="查看創作者介紹">
                        <!-- 建議尺寸 1010 * 1010 -->
                        <img src="images/newcreator01.png" alt="創作者照片" class="pagCrelist-imgBk--img">
                    </a>
                </li>
            </ul>
        </div>
        <!-- 分頁區 -->
        <div class="eleSelPageBk pagGalliPageBk">
            <button class="eleSelPageArrow eleSelPageArrow--left "></button>
            <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount eleSelPageCount--pageIn mlr-3">01</a>
            <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-3">02</a>
            <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-3">03</a>
            <a href="javascript:void(0);" title="第一頁" class="eleSelPageCount mlr-3">04</a>
            <button class="eleSelPageArrow eleSelPageArrow--right"></button>
        </div>
        
        
    </div>

    
    
    <?php require('newfooter.php') ?>
        

</body>
</html>

     