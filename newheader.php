<a href="index.php" class="patLogo js-headerLogo" title="回小光點畫廊首頁">
	<img src="images/newlogo-word.svg" alt="小光點藝廊Logo word" class="patLogo-circle">
	<img src="images/newlogo.svg" alt="小光點藝廊LOGO" class="patLogo-logo">
</a>
<div class="patheaderBk js-patheader">
	<button class="patheaderBt js-navOpenBt" aria-label="打開網站連結選單">
		<img src="images/newnav-open-bt.svg" alt="icon" class="patheaderBt-icon">
	</button>
	<!-- 連結 -->
	<section class="patheaderLink js-patheaderLink">
		<a href="galleryli.php" class="patheaderLink-link">線上畫廊</a>
		<a href="proli.php" class="patheaderLink-link">周邊商品</a>
		<a href="creatorli.php" class="patheaderLink-link">創作者介紹</a>
		<a href="actli.php" class="patheaderLink-link">展覽活動</a>
	</section>
</div>

<div class="patnavBk js-patnavBk">
	<div class="patnav js-patnavContent">
		<div class="max_width">
			
			<div class="patnav-head">
				<button class="patnav-head--navCloseBt js-navCloseBt" aria-label="關閉網站連結選單">
					<img src="images/newnav-close-bt.svg" alt="icon" class="patnav-head--navCloseBt--icon">
				</button>
			</div>

			<div class="patnavSection">
				<section class="patnavFirstArea patnavFirstArea--noIcon">
					<h6 class="patnavFirstArea-tit mb-12">
						Main function
					</h6>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">線上畫廊</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
					<a href="proli.php" class="patnavSecondArea-link">周邊商品</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="creatorli.php" class="patnavSecondArea-link">創作者介紹</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="actli.php" class="patnavSecondArea-link">展覽活動</a>
					</li>
					<br />
					<br />
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="login.php" class="patnavSecondArea-link patnavFirstArea--embt">會員註冊／登入</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="cart01.php" class="patnavSecondArea-link patnavFirstArea--embt">購物車</a>
					</li>
				</section>	
			</div>

			<div class="patnavSection">
				<section class="patnavFirstArea patnavFirstArea--noIcon">
					<h6 class="patnavFirstArea-tit mb-12">
						Others
					</h6>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="about.php" class="patnavSecondArea-link">關於我們</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
					<a href="contact.php" class="patnavSecondArea-link">連絡我們</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="wanted.php" class="patnavSecondArea-link">招募夥伴</a>
					</li>
				</section>	
			</div>

			<div class="patnavSection">
				<section class="patnavFirstArea patnavFirstArea--noIcon">
					<h6 class="patnavFirstArea-tit mb-12">
						Sort by material
					</h6>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">油畫</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">水彩</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">壓克力顏料</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">粉蠟筆</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">綜合媒材</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">色鉛筆</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">麥克筆</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">廣告顏料</a>
					</li>
				</section>	
			</div>

			<div class="patnavSection">
				<section class="patnavFirstArea patnavFirstArea--noIcon">
					<h6 class="patnavFirstArea-tit mb-12">
						Sort by price
					</h6>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">$ 3,000 － $ 10,000</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">$ 10,000 － $ 20,000</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">$ 20,000 － $ 30,000</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">$ 30,000 － $ 50,000</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">$ 50,000 － $ 80,000</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">$ 80,000 － $ 100,000</a>
					</li>
					<li class="patnavSecondArea-li patnavSecondArea--noIcon">
						<a href="galleryli.php" class="patnavSecondArea-link">$ 100,000 －</a>
					</li>
				</section>	
			</div>
			<br />
			<br />
			<br />
		</div>
	</div>
	<div class="">
		<div class="patnavBg js-navBg"></div>
	</div>

</div>