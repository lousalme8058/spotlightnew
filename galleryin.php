<!DOCTYPE html>	
<head>
<title>小光點藝廊</title>
<!-- 社群連結fb/line -->
<meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="小光點畫廊 Spotlight gallery" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<!-- 抓畫作圖 -->
<meta property="og:image" content="" />
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="1440" />
<meta property="og:image:height" content="auto" />

<?php require('newhead.php') ?>

<!-- JSON-LD 結構化資料 --產品 -->
<script type="application/ld+json">
{
    "@context": "https://schema.org/",
    "@type": "Product",
    //產品名
    "name": "Executive Anvil",
    //產品照片
    "image": [
        "https://example.com/photos/1x1/photo.jpg"
    ],
    //產品編號
    "sku": "0446310786",
    "brand": {
        "@type": "Brand",
        "name": "小光點畫廊"
    },
    //產品描述
    "description": "Sleeker than ACME's Classic Anvil, the Executive Anvil is perfect for the business traveler looking for something to drop from a height.",
    "offers": {
        "@type": "Offer",
        //產品網址
        "url": "https://example.com/anvil",
        //產品貨幣單位
        "priceCurrency": "USD",
        //價格
        "price": "119.99",
        //庫存
        "availability": "http://www.spotlight-gallery.com.tw/InStock"
    }
}
</script>
<script language="javascript">
// 動畫效果
$(document).ready(function() {

});
$(window).on('load',function(){
    
    var imgWidth = $(".js-paintingImg").width();
    console.log("圖片寬:" + imgWidth);
    var imgHeight = $(".js-paintingImg").height();
    console.log("圖片高:" + imgHeight);
    
    if($(window).width() >= 1800 && imgWidth > imgHeight){
        $(".js-paintingBk").css({"width":"90%","height":"auto"});
        $(".js-paintingImg").css({"width":"100%","height":"auto"});
        console.log("寬度為85%");
    }else if($(window).width() >= 1024 && imgWidth > imgHeight){
        $(".js-paintingBk").css({"width":"100%","height":"auto"});
        $(".js-paintingImg").css({"width":"100%","height":"auto"});
        console.log("寬度為100%");
    }else if($(window).width() >= 1024 && imgWidth < imgHeight){
        $(".js-paintingBk").css({"width":"auto","height":"80%"});
        $(".js-paintingImg").css({"width":"auto","height":"100%"});
        console.log("高度為70%");
    };

    if($(window).width() >= 1024){
        
        var ImgHeight = $(".js-paintingImg").innerHeight();
        var bgHeight = $(".js-paintingBg").innerHeight() - 60;
        console.log("最後圖片高:" + ImgHeight);
        console.log("背景內距:" + bgHeight);
        $(".js-buyInfoBg").outerHeight(bgHeight);
    };

	TweenMax.to(".js-decLine", 1, { delay: 2.5, bottom:"-100px", height:"220px"  ,opacity:"1" , ease: Power4.inOut });

    // 已售出標籤
    TweenMax.to(".js-soldLabel", 0.5, { delay: 1.6, scaleX:"1", scaleY:"1", transform: "rotate(-15deg)" ,opacity:"1" ,   ease: "elastic.inOut(1, 0.3)"});

});
</script>
<body>
    <!-- loading動畫 -->
    <div class="js-patLoadingAniBg patLoading">
    </div>

    <?php require('newheader.php') ?>

    <div class="pagGalinBk">
        <!-- 畫作圖片區 -->
        <article class="pagGalinPaintingBK js-paintingBg">
            <a href="#pagGalinImgZoom" class="pagGalinPaintingBK-zoomBtBk" title="放大觀看畫作">
                <img src="images/newzoom-in-icon.svg" alt="icon" class="pagGalinPaintingBK-zoomBtBk--icon">
                <p class="pagGalinPaintingBK-zoomBtBk--text">放大畫作觀看</p>     
            </a>
            <a href="#pagGalinImgZoom" class="js-paintingBk pagGalinPainting-imgBk">
                <img src="images/newgallery06.png" alt="畫作名稱" class="js-paintingImg pagGalinPainting-imgBk--img">
            </a>
        </article>
        <article class="pagGaliBuyInfoBk js-buyInfoBg">
            <h1 class="patSection pagGaliBuyInfoBk-content mb-15">清風中的夏蓮</h1>
            <h2 class="pagGaliBuyInfoBk-content pagGaliBuyInfoBk-content--price">$10,800</h2>
            <!-- 售出標籤 -->
            <!-- <div class="js-soldLabel pagGaliBuyInfoBk--soldLabel">
                <h5 class="pagGaliBuyInfoBk--soldLabel--en mb-5">Sold out</h5>
                <h6 class="pagGaliBuyInfoBk--soldLabel--ch">畫作已售出</h6>
            </div> -->
            <div class="pagGaliBuyInfoBk-content pagGaliBuyInfoBk-content--buyBt">
                <a href="javascript:void(0);" class="btnBuyBt" title="購買畫作">
                    購買畫作
                </a>
            </div>
            <div class="pagGaliBuyInfoBk--decLine js-decLine"></div>
        </article>


        <!-- 畫作資訊區 -->
        <div class="patSection pagGalinLeftBk">      
            <!-- 畫家資訊 -->
            <article class="pagGalinLeft-paintingInfo">
                <div class="mod-decTitArea of-hidden pagGalinLeft-paintingInfo--bk">
                    <h4 class="mod-decTitArea--tit mt-5">Info</h4>
                    <div class="mod-decTitArea--content">
                        <div class="pagGalinLeft-paintingInfo--column">
                            <h5 class="mb-5">Artist</h5>
                            <h6 class="mb-15 pr-20">鄭美珠－輕度魚鱗癬與顱顏併發症</h6>
                            <h5 class="mb-5">Material</h5>
                            <h6 class="mb-15 pr-20">油畫</h6>
                        </div>
                        <div class="pagGalinLeft-paintingInfo--column">
                            <h5 class="mb-5">Years</h5>
                            <h6 class="mb-15 pr-20">2018</h6>
                            <h5 class="mb-5">Size</h5>
                            <h6 class="mb-15 pr-20">8F (45.5 × 38 cm)</h6>
                        </div>
                    </div>
                </div>
                <div class="mod-decTitArea pagGalinLeft-paintingInfo--bk">
                    <h4 class="mod-decTitArea--tit mt-5">Concept</h4>
                    <div class="mod-decTitArea--content">
                        <p class="">
                            家是我擁有溫馨回憶的地方，在我父親過世後，家更成為我收集回憶之地，把它放在畫中，想告訴大家珍惜和家人相處的時光。
                        </p>
                    </div>
                </div>
                <div class="mod-decTitArea pagGalinLeft-paintingInfo--bk">
                    <h4 class="mod-decTitArea--tit mt-5">Creator</h4>
                    <div class="mod-decTitArea--content pagGalinLeft-paintingInfo--creatorBk">
                       <a href="creatorli.php" class="pagGalinCreatorImgBk" title="創作者陳沛儒介紹">
                            <img src="images/newcreator01.png" alt="創作者照片" class="pagGalinCreatorImgBk--Img">
                       </a>
                       <h2 class="pagGalinCreatorName">
                            <a href="creatorin.php" class="pagGalinCreatorName-a" title="創作者陳沛儒介紹">
                                陳沛儒
                            </a>
                       </h2>
                    </div>
                </div>
            </article>
        </div>
        <div class="patSection mt-50 mb-50">
            <!-- 麵包屑 -->
            <article class="eleBite mb-15">
                <a href="index.php" class="eleBite-link">
                    首頁
                </a>
                <a href="galleryli.php" class="eleBite-link">
                    線上畫廊
                </a>
                <a href="index.php" class="eleBite-link">
                    油畫
                </a>
            </article>
        </div>
    </div>

    <!-- 畫作放大區 -->
    <div class="modLightbox" id="pagGalinImgZoom">
        <div class="modLightbox-contentBk">
            <a href="#" class="modLightbox-close"></a>
            <div class="modLightbox-content ">
                <!-- <img src="images/newgallery010.png" alt="畫作名稱" class="" width="100%"> -->
                <!-- <img src="images/newgallery07.png" alt="畫作名稱" title="畫作" class=""  width="100%"> -->
                <img src="images/newgallery09.png" alt="畫作名稱" title="畫作" class="" width="100%">
            </div>
            
        </div>
    </div>
    
    <?php require('newfooter.php') ?>
        

</body>
</html>
